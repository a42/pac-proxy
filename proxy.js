/* 
	proxy.js: PAC file aware HTTP proxy utility - Main program

	Copyright (c) Jozsef Orosz - jozsef@orosz.name

*/


var PACProxy = require('./lib/pac-proxy'),
	winston   = require('winston');

	
winston.cli();
winston.level = 'debug';


var options = { 
		port: 8080,
		pac: 'http://192.168.3.2:8000/test.pac'
};

var proxy = PACProxy.createProxy(options);
	
proxy.start();
